package Q2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Contact {
	private double balance;
	private Lock BalanceChangedLock;
	private Condition enoughFundCondition;
	private double interestrate;
	private Queue qu;

	public Contact() {

		BalanceChangedLock = new ReentrantLock();
		enoughFundCondition = BalanceChangedLock.newCondition();
		qu = new Queue(10);
	}

	public void add(String amount) throws InterruptedException {
		try {
			BalanceChangedLock.lock();

			while (qu.checkSize() > 10) {
				enoughFundCondition.await();
			}
			qu.add(amount);

			System.out.print("Add " + amount+"\n");
			System.out.println(qu.toString());
			enoughFundCondition.signalAll();
		} finally {
			BalanceChangedLock.unlock();
		}
	}

	public void remove(String amount) throws InterruptedException {
		try {
			BalanceChangedLock.lock();
			while (qu.checkSize() <= 0) {
				enoughFundCondition.await();
			}
			qu.remove(0);
			System.out.print("remove " + amount+"\n");
			System.out.println(qu.toString());
			enoughFundCondition.signalAll();
		} finally {
			BalanceChangedLock.unlock();
		}

	}

}
